# numahell2015, based on Flex

A minimalist [Pelican](http://blog.getpelican.com/) theme. 
Based on [Flex theme](https://github.com/alexandrevicenzi/Flex).

## Install

It uses Sass and can be generate by node-sass

```
npm run build
```

## License

MIT
